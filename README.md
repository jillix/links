# Links

Mono module that will show/remove HTML elements using the login module data.

# Changelog

### v0.1.4
 - Check if `userInfo` exists and only if it does remove the elements.

### v0.1.3
 - Use `config.options.prefix` to set the prefix of role class (default: `"role-"`);
 - Update to Bind v0.2.1 and Events v.0.1.7
 - Added the `processConfig` function
 - Added comments to understand the code better

### v0.1.2
 - Don't remove the role from the role array if data is `undefined`

### v0.1.1
 - Emit `ready` event.

### v0.1.0
 - Initial release.
